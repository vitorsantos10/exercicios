
public class Piriquito extends Passaro{
	private boolean domestico;

	public boolean isDomestico() {
		return domestico;
	}

	public void setDomestico(boolean domestico) {
		this.domestico = domestico;
	}
	
	@Override
	public String toString() {
		return String.valueOf(isDomestico());
				
		
	}

	@Override
	public String fazBarulho() {
		// TODO Auto-generated method stub
		return " piu-piu ";
	}
}
