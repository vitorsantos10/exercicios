
public abstract class Mamifero extends Animal{

	private boolean mama;
	private boolean produzLeite;
	private boolean quadrupte;
	
	
	public boolean isMama() {
		return mama;
	}
	public void setMama(boolean mama) {
		this.mama = mama;
	}
	public boolean isProduzLeite() {
		return produzLeite;
	}
	public void setProduzLeite(boolean produzLeite) {
		this.produzLeite = produzLeite;
	}
	public boolean isQuadrupte() {
		return quadrupte;
	}
	public void setQuadrupte(boolean quadrupte) {
		this.quadrupte = quadrupte;
	}
	
}
