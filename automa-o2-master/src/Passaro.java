
public  abstract class Passaro extends Animal{
	private boolean voa;
	private boolean assovia;
	private boolean dancaAcasalamento;
	
	public boolean isVoa() {
		return voa;
	}
	public void setVoa(boolean voa) {
		this.voa = voa;
	}
	public boolean isAssovia() {
		return assovia;
	}
	public void setAssovia(boolean assovia) {
		this.assovia = assovia;
	}
	public boolean isDancaAcasalamento() {
		return dancaAcasalamento;
	}
	public void setDancaAcasalamento(boolean dancaAcasalamento) {
		this.dancaAcasalamento = dancaAcasalamento;
	}
	
}
