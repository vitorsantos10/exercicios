
public class Principal {
	public static void main(String[] args) {

		
		System.out.println("------------------------------ CONTA CORRENTE---------------------------");
		ContaCorrente c = new ContaCorrente();
		c.setSaldo(150);
        System.out.println("O Seu saldo na conta corrente � " + c.saque(c.getSaldo(), 20));
        System.out.println();
		   
		System.out.println("--,---------------------------- CONTA POUPAN�A---------------------------");
		ContaPoupan�a p = new ContaPoupan�a();
		p.setSaldo(300);
		System.out.println("O Seu saldo na conta � Poupan�a � " + p.saque(p.getSaldo(), 250));
		System.out.println();
		
		
		System.out.println("------------------------------ CONTA SALARIO---------------------------");
		ContaSalario s = new ContaSalario();
		s.setSaldo(300);
		System.out.println("O Seu saldo na conta Salario � " + s.saque(s.getSaldo(), 10));
		System.out.println();
	}

}