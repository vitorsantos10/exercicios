import java.util.Scanner;

public class RegrasJogo {
	
		Scanner linhas = new Scanner(System.in);
		Corpo c;
		String[] vetor = new String[8];
		String vencedor = "null";
		int rodadas = 0;
			
		public RegrasJogo(Corpo corpo) {
			c = corpo;
		}
		
		public void jogadaValida(int linha, int coluna, JogadorSinal jogador) {
			if (c.getMatriz()[linha][coluna].equals("_")) {
				c.inserirMatriz(linha, coluna, jogador.getSinal());
			} else {
				System.out.println(" ");
				System.out.println("JOGADA INVALIDA !!!!!!!");
			}
		}
		
		public void jogada(int linha, int coluna, JogadorSinal jogador) {
			
			System.out.println(" ");
			System.out.println("Qual posi��o deseja jogar? ");

			System.out.println("Linha");
			linha = linhas.nextInt();


			System.out.println("Coluna");
			coluna = linhas.nextInt();
			
			jogadaValida(linha, coluna, jogador);
//			c.inserirMatriz(linha, coluna, jogador.getSinal());
		}
		
		public String ganhou(int rodadas) {
			
			// Horizontal
			vetor[0] = c.getMatriz()[0][0] +c.getMatriz()[0][1] + c.getMatriz()[0][2];
			vetor[1] = c.getMatriz()[1][0] + c.getMatriz()[1][1] + c.getMatriz()[1][2];
			vetor[2] = c.getMatriz()[2][0] + c.getMatriz()[2][1] + c.getMatriz()[2][2];

			// Vertical
			vetor[3] = c.getMatriz()[0][0] + c.getMatriz()[1][0] + c.getMatriz()[2][0];
			vetor[4] = c.getMatriz()[0][1] + c.getMatriz()[1][1] + c.getMatriz()[2][1];
			vetor[5] = c.getMatriz()[0][2] + c.getMatriz()[1][2] + c.getMatriz()[2][2];

			// Diagonal
			vetor[6] = c.getMatriz()[0][0] + c.getMatriz()[1][1] + c.getMatriz()[2][2];
			vetor[7] = c.getMatriz()[0][2] + c.getMatriz()[1][1] + c.getMatriz()[2][0];

			for (int j = 0; j < vetor.length; j++) {
				if (vetor[j].equals("XXX")) {
					vencedor = "jogador 1";
					System.out.println("JOGADOR 1 VENCEU !!!!!");
					break;
				} else if (vetor[j].equals("OOO")) {
					vencedor = "jogador 2";
					System.out.println("JOGADOR 2 VENCEU !!!!!");
					break;
				}
				if (rodadas == 8) {
					vencedor = "EMPATE !!!";
					System.out.println("EMPATE !!!");
				}
				
			}
			return vencedor;
		}
		
		public void jogo(JogadorSinal jogador1, JogadorSinal jogador2) {
			while(true) {
				c.exibir();
				jogador1.setSinal("X");
				jogada(0, 0, jogador1);			
				
				if (!ganhou(rodadas).equals("null")) {
					c.exibir();
					break;
				}
				rodadas++;
				
				c.exibir();
				jogador2.setSinal("O");
				jogada(0, 0, jogador2);
				
				if (!ganhou(rodadas).equals("null")) {
					c.exibir();
					break;
				}

				c.exibir();
				rodadas++;
		}
		}
			}


