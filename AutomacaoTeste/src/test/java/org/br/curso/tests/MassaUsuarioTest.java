package org.br.curso.tests;

import java.lang.reflect.Method;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import static br.com.santander.frm.bdd.Gherkin.executeScenario_;

import br.com.santander.frm.base.TestBase;
import br.com.santander.frm.controllers.WebController;
import br.com.santander.frm.testng.DataTableConfig;
import br.com.santander.frm.testng.TestConfig;

@TestConfig(controllerType = WebController.class, dtFilePropertyAlias = "env.dt.massa")
public class MassaUsuarioTest extends TestBase {
	@DataTableConfig(ct = 1) // caso de teste da masssa
	@Test(groups = { "Massa" }, priority = 1, testName = "CT002-Massa")
	public void executar_login_valido() {
		try {
			executeScenario_("Massa", "CT002-Massa");
		} catch (Exception e) {
			Assert.fail("Test error ", e);
		}
	}

	@BeforeMethod(alwaysRun = true)
	public void setup(final Method method, final ITestContext context) {
		super.setup(method, context);
	}

	@AfterMethod(alwaysRun = true)
	public void teardawon(final Method method, final ITestContext context, ITestResult testResult) {
		super.teardown(method, context, testResult);
	}
}
