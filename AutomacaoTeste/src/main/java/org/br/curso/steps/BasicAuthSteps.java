package org.br.curso.steps;

import static br.com.santander.frm.helpers.DataTableHelper.getDt_;
import static br.com.santander.frm.base.DefaultBaseController.getPage_;
import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;

import java.awt.AWTException;

import org.br.curso.pages.BasicAuthPage;
import org.br.curso.pages.BatistaHomePage;
import org.br.curso.pages.CriarUsuarioPage;
import org.br.curso.pages.ValidarUsuarioPage;

import br.com.santander.frm.bdd.DesignSteps;
import br.com.santander.frm.bdd.Step;
import br.com.santander.frm.exceptions.ElementFindException;
import br.com.santander.frm.exceptions.GenericException;

@DesignSteps
public class BasicAuthSteps {

	BatistaHomePage batistahomePage = getPage_(BatistaHomePage.class);
	BasicAuthPage basicauthPage = getPage_(BasicAuthPage.class);

	@Step("Dado que estou na HOME batista ")
	public void Dado_que_estou_na_HOME_batista() throws ElementFindException {
		batistahomePage.validar_home();
		batistahomePage.clicar_outros();
		batistahomePage.clicar_auth();

	}

	@Step("Quando que os campos foram preenchidos")
	public void Quando_que_os_campos_foram_preenchidos() throws AWTException {
		basicauthPage.interagir_chrome();

	}

	@Step("Ent�o o usuario sera autenticado")
	public void Ent�o_o_usuario_sera_autenticado() throws ElementFindException {
		basicauthPage.validar_autenticacao();
	}

}