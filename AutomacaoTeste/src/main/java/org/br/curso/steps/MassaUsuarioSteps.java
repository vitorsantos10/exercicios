package org.br.curso.steps;

import static br.com.santander.frm.helpers.DataTableHelper.getDt_;
import static br.com.santander.frm.base.DefaultBaseController.getPage_;
import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;

import org.br.curso.pages.BatistaHomePage;
import org.br.curso.pages.CriarUsuarioPage;
import org.br.curso.pages.MassaUsuarioPage;

import br.com.santander.frm.bdd.DesignSteps;
import br.com.santander.frm.bdd.Step;
import br.com.santander.frm.exceptions.ElementFindException;

@DesignSteps
public class MassaUsuarioSteps {
	BatistaHomePage batistahomePage = getPage_(BatistaHomePage.class);
	MassaUsuarioPage massausuarioPage = getPage_(MassaUsuarioPage.class);

	@Step("Dado que estou na p�gina Automacao Basticaa")
	public void Dado_que_estou_na_p�gina_Automacao_Bastica() throws ElementFindException {
		batistahomePage.validar_home();

	}

	@Step("Quando clicar em inputs e preencher campos")
	public void Quando_clicar_em_inputs_e_preencher_campos() throws ElementFindException {
		batistahomePage.clicar_busca();
		batistahomePage.clicar_input();
		massausuarioPage.inserir_informacoes(getDt_().getStringOf("FirtName"), getDt_().getStringOf("LastName"),
				getDt_().getStringOf("Password"), getDt_().getStringOf("Email"), getDt_().getStringOf("Text"));

	}

}
