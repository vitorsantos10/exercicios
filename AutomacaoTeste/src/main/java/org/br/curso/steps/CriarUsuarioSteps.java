package org.br.curso.steps;
import static br.com.santander.frm.helpers.DataTableHelper.getDt_;
import static br.com.santander.frm.base.DefaultBaseController.getPage_;
import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;

import org.br.curso.pages.BatistaHomePage;
import org.br.curso.pages.CriarUsuarioPage;
import org.br.curso.pages.ValidarUsuarioPage;

import br.com.santander.frm.bdd.DesignSteps;
import br.com.santander.frm.bdd.Step;
import br.com.santander.frm.exceptions.ElementFindException;

@DesignSteps
public class CriarUsuarioSteps {

	BatistaHomePage batistahomePage = getPage_(BatistaHomePage.class);
	CriarUsuarioPage criarusuarioPage = getPage_(CriarUsuarioPage.class);
	ValidarUsuarioPage validarUsuarioPage = getPage_(ValidarUsuarioPage.class);

	@Step("Dado que estou na p�gina Automacao Bastica")
	public void Dado_que_estou_na_p�gina_Automacao_Bastica() throws ElementFindException {
		batistahomePage.validar_home();

	}

	@Step("Quando clicar em usuario e preencher os campos")
	public void Quando_clicar_em_usuario_e_preencher_os_campos() throws ElementFindException {
		batistahomePage.clicar_em_formulario();
		batistahomePage.clicar_em_criarUsuario();
		criarusuarioPage.preencher_formulario(getDt_().getStringOf("name"),getDt_().getStringOf("ultimonome"),getDt_().getStringOf("email")
				, getDt_().getStringOf("endereco"), getDt_().getStringOf("universidade"),getDt_().getStringOf("profissao"),
				getDt_().getStringOf("genero"),getDt_().getStringOf("idade"));

	}

	@Step("Ent�o o usuario sera criado")
	public void Ent�o_o_usuario_sera_criado() throws ElementFindException {
		validarUsuarioPage.validarCadastro();
	}

}