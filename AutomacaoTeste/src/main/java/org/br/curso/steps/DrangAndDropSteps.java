package org.br.curso.steps;

import static br.com.santander.frm.helpers.DataTableHelper.getDt_;
import static br.com.santander.frm.base.DefaultBaseController.getPage_;
import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;

import org.br.curso.pages.BatistaHomePage;
import org.br.curso.pages.DragAndDropPage;

import br.com.santander.frm.bdd.DesignSteps;
import br.com.santander.frm.bdd.Step;
import br.com.santander.frm.exceptions.ElementFindException;
import br.com.santander.frm.exceptions.GenericException;

@DesignSteps
public class DrangAndDropSteps {

	BatistaHomePage batistahomePage = getPage_(BatistaHomePage.class);
	DragAndDropPage draganddropPage = getPage_(DragAndDropPage.class);

	@Step("Dado que acessei a home")
	public void Dado_que_acessei_a_home() {
		batistahomePage.validar_home();

	}

	@Step("Quando clicar em interacoes")
	public void Quando_clicar_em_interacoes() throws ElementFindException {
		batistahomePage.ir_drag();
		batistahomePage.clicar_drop();
		draganddropPage.validar_DRAG();
	}

	@Step("Ent�o fazer DragAndDrop")
	public void Ent�o_fazer_DragAndDrop() throws ElementFindException, GenericException {
		draganddropPage.arrastar_wilson();
		draganddropPage.validar_arrastao();

	}

}
