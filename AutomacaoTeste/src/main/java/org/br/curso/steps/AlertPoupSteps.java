package org.br.curso.steps;

import static br.com.santander.frm.helpers.DataTableHelper.getDt_;
import static br.com.santander.frm.base.DefaultBaseController.getPage_;
import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;

import org.br.curso.pages.BatistaHomePage;
import org.br.curso.pages.HomeAlertPage;

import br.com.santander.frm.bdd.DesignSteps;
import br.com.santander.frm.bdd.Step;
import br.com.santander.frm.exceptions.ElementFindException;
import br.com.santander.frm.exceptions.GenericException;

@DesignSteps
public class AlertPoupSteps {

	BatistaHomePage batistahomePage = getPage_(BatistaHomePage.class);
	HomeAlertPage homealertPage = getPage_(HomeAlertPage.class);

	@Step("Dado que estou na home ")
	public void Dado_que_estou_na_home() {
		batistahomePage.validar_home();

	}

	@Step("Quando clicar em alert")
	public void Quando_clicar_em_alert() throws ElementFindException {
		batistahomePage.clicar_mudanca();
		batistahomePage.clicar_alert();
	}

	@Step("Ent�o interagir com os alerts")
	public void Ent�o_interagir_com_os_alerts() throws ElementFindException, GenericException {
		homealertPage.clicar_alert();
		homealertPage.clicar_confirm();
		homealertPage.cliar_escrever();

	}

}
