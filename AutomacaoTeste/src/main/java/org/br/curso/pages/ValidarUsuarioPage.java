package org.br.curso.pages;

import br.com.santander.frm.base.PageBase;
import br.com.santander.frm.base.VirtualElement;
import br.com.santander.frm.exceptions.ElementFindException;
import br.com.santander.frm.helpers.LoggerHelper;

import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;

import org.testng.Assert;;

public class ValidarUsuarioPage extends PageBase {

	VirtualElement validar = getElementByXPath("//p[@id = \"notice\"]");

	public void validarCadastro() {
		Assert.assertTrue(elementExists(validar));
		logger.info("Usuario foi Cadastrado.", true);
	}
}
