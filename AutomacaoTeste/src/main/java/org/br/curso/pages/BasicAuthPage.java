package org.br.curso.pages;

import br.com.santander.frm.base.PageBase;
import br.com.santander.frm.base.VirtualElement;
import br.com.santander.frm.exceptions.ElementFindException;
import br.com.santander.frm.exceptions.GenericException;
import br.com.santander.frm.helpers.LoggerHelper;

import java.awt.AWTException;
import java.awt.Robot;

import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;

import java.awt.event.KeyEvent;

import org.testng.Assert;

public class BasicAuthPage extends PageBase {
	VirtualElement validar = getElementByXPath("//h5[text()= 'Voc� se autenticou com sucesso!!']");

	private Robot robot;

	public void interagir_chrome() throws AWTException {
		robot = new Robot();
		robot.setAutoDelay(300);

		robot.keyPress(KeyEvent.VK_A);
		robot.keyPress(KeyEvent.VK_D);
		robot.keyPress(KeyEvent.VK_M);
		robot.keyPress(KeyEvent.VK_I);
		robot.keyPress(KeyEvent.VK_N);
		logger.info("Campos preenchidos", true);
		robot.keyPress(KeyEvent.VK_TAB);
		robot.keyPress(KeyEvent.VK_A);
		robot.keyPress(KeyEvent.VK_D);
		robot.keyPress(KeyEvent.VK_M);
		robot.keyPress(KeyEvent.VK_I);
		robot.keyPress(KeyEvent.VK_N);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyPress(KeyEvent.VK_CAPS_LOCK);
		logger.info("Campos preenchidos", true);

	}

	public void validar_autenticacao() throws ElementFindException {
		Assert.assertTrue(elementExists(validar));
		logger.info("P�gina inicial foi encontrada.", true);
	}

}