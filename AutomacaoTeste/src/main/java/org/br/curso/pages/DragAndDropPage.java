package org.br.curso.pages;

import br.com.santander.frm.base.PageBase;
import br.com.santander.frm.base.VirtualElement;
import br.com.santander.frm.exceptions.ElementFindException;
import br.com.santander.frm.exceptions.GenericException;
import br.com.santander.frm.helpers.LoggerHelper;
import static br.com.santander.frm.base.DefaultBaseController.getDriver_;

import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;

import org.openqa.selenium.Alert;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

public class DragAndDropPage extends PageBase {

	VirtualElement DragAndHome = getElementByXPath("//h5[text() = 'Drag and Drop!!']"),
			SrWilson = getElementByXPath("//img[@id = 'winston']"),
			CasaWilson = getElementByXPath("//div[@id = 'dropzone']"),
			validar = getElementByXPath("//div[@class = 'col s4']");

	public void validar_DRAG() {
		Assert.assertTrue(elementExists(DragAndHome));
		logger.info("P�gina inicial foi encontrada.", true);
	}

	public void arrastar_wilson() throws ElementFindException, GenericException {

		Actions act = new Actions(getDriver_());

		act.dragAndDrop(SrWilson.get(), CasaWilson.get()).build().perform();

	}

	public void validar_arrastao() {
		Assert.assertTrue(elementExists(validar));
		logger.info("P�gina inicial foi encontrada.", true);
	}
}
