package org.br.curso.pages;

import br.com.santander.frm.base.PageBase;
import br.com.santander.frm.base.VirtualElement;
import br.com.santander.frm.exceptions.ElementFindException;
import br.com.santander.frm.helpers.LoggerHelper;

import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;

import org.testng.Assert;

public class MassaUsuarioPage extends PageBase {

	VirtualElement header = getElementByXPath("//h5[text() = 'Inputs, TextField e TextArea']"),
			firstname = getElementByXPath("//input[@id = 'first_name']"),
			lastname = getElementByXPath("//input[@id = 'last_name']"),
			password = getElementByXPath("//input[@id = 'password']"),
			email = getElementByXPath("//input[@id = 'email']"),
			text = getElementByXPath("//textarea[@id = 'textarea1']");

	public void validar_header() {

		Assert.assertTrue(elementExists(header));
		logger.info("P�gina inicial foi encontrada.", true);
	}

	public void inserir_informacoes(String FirtName, String LastName, String Password, String Email, String Text)
			throws ElementFindException {

		if (elementExists(header) && elementExists(firstname) && elementExists(lastname) && elementExists(password)
				&& elementExists(email) && elementExists(text))
			;

		firstname.sendKeys(FirtName);
		lastname.sendKeys(LastName);
		password.sendKeys(Password);
		email.sendKeys(Email);
		text.sendKeys(Text);

		logger.info("Campos preenchidos", true);

	}

}
