package org.br.curso.pages;

import br.com.santander.frm.base.PageBase;
import br.com.santander.frm.base.VirtualElement;
import br.com.santander.frm.exceptions.ElementFindException;
import br.com.santander.frm.helpers.LoggerHelper;

import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;

import org.testng.Assert;

public class BatistaHomePage extends PageBase {

	VirtualElement homePage = getElementByXPath("//h5[@class = \"orange-text center \"]"),
			formularioPage = getElementByXPath("//a[@class = 'collapsible-header ']"),
			criarUsuarioPage = getElementByXPath("//a[@href = \"/users/new\"]"),
			BuscaElement = getElementByXPath("//a[text() = 'Busca de elementos']"),
			inputs = getElementByXPath("//a[text() = 'Inputs e TextField']"),
			mudancaFoco = getElementByXPath("//a[text() = 'Mudan�a de foco']"),
			Alert = getElementByXPath("//a[text() = 'Alert']"),
			interacoes = getElementByXPath("//a[text() = 'Itera��es']"),
			DragAndDrop = getElementByXPath("//a[text() = 'Drag And Drop']"),
			Outros = getElementByXPath("//a[text()= 'Outros']"),
			hrefbasic = getElementByXPath("//a[text()='Basic Auth(user:admin, password: admin)']");

	LoggerHelper logger = new LoggerHelper(BatistaHomePage.class);

	public void validar_home() {
		Assert.assertTrue(elementExists(homePage));
		logger.info("P�gina inicial foi encontrada.", true);
	}

	// CENARIOOOO CRIAR USUARIO
	public void clicar_em_formulario() throws ElementFindException {
		formularioPage.click();
		waitUntilElementExists(criarUsuarioPage);
	}

	public void clicar_em_criarUsuario() throws ElementFindException {
		criarUsuarioPage.click();
	}
	// -------------------------------------------------------------------

	// CENARIOOOO MASSA
	public void clicar_busca() throws ElementFindException {
		BuscaElement.click();
		waitUntilElementExists(inputs);
	}

	public void clicar_input() throws ElementFindException {
		inputs.click();
	}

	// ---------------------------------------------------------------------

	// CENARIOOOOO TESTE DOS ALERTS

	public void clicar_mudanca() throws ElementFindException {
		waitUntilElementExists(mudancaFoco);
		mudancaFoco.click();
		waitUntilElementExists(Alert);
	}

	public void clicar_alert() throws ElementFindException {
		Alert.click();
	}

	// ------------------------------------------------------------------------

	// CENARIOOOOO DRAG AND DROP

	public void ir_drag() throws ElementFindException {
		waitUntilElementExists(interacoes);
		interacoes.click();

	}

	public void clicar_drop() throws ElementFindException {
		waitUntilElementExists(DragAndDrop);
		DragAndDrop.click();
	}

	// -----------------------------------------------------------------------------------

	// CENARIOOOOO AUTH BASIC

	public void clicar_outros() throws ElementFindException {
		waitUntilElementExists(Outros);
		Outros.click();

	}

	public void clicar_auth() throws ElementFindException {
		waitUntilElementBeClickable(hrefbasic);
		hrefbasic.click();
		sleep(3000);
	}
	// ----------------------------------------------------------------------------------
}
