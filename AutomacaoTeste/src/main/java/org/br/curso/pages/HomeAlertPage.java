package org.br.curso.pages;

import br.com.santander.frm.base.PageBase;
import br.com.santander.frm.base.VirtualElement;
import br.com.santander.frm.exceptions.ElementFindException;
import br.com.santander.frm.exceptions.GenericException;
import br.com.santander.frm.helpers.LoggerHelper;
import static br.com.santander.frm.base.DefaultBaseController.getDriver_;

import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;

import org.openqa.selenium.Alert;
import org.testng.Assert;

public class HomeAlertPage extends PageBase {

	VirtualElement HomeALert = getElementByXPath("//h3[text() = 'JavaScript Alerts']"),
			btnJsAlert = getElementByXPath("//button[text() = 'Clique para JS Alert']"),
			btnJsConfirm = getElementByXPath("//button[text() = 'Clique para JS Confirm']"),
			btnJsPromt = getElementByXPath("//button[text() = 'Clique para JS Prompt']"),
			result = getElementByXPath("//p[text() = 'Voc� clicou no alerta com sucesso!!']"),
			resultCancel = getElementByXPath("//p[text() = 'Voc� clicou: Cancel']"),
			resultResporta = getElementByXPath("//p[text() = 'Voc� clicou: vitor']");

	public void validar_homealert() {
		Assert.assertTrue(elementExists(HomeALert));
		logger.info("P�gina inicial foi encontrada.", true);
	}

	public void clicar_alert() throws ElementFindException, GenericException {
		waitUntilElementExists(btnJsAlert);
		btnJsAlert.click();
		getDriver_().switchTo().alert().accept();
		Assert.assertTrue(elementExists(result));
		logger.info("foi feito o Click", true);

	}

	public void clicar_confirm() throws ElementFindException, GenericException {
		waitUntilElementExists(btnJsConfirm);
		btnJsConfirm.click();
		getDriver_().switchTo().alert().dismiss();
		Assert.assertTrue(elementExists(resultCancel));
		logger.info("foi feito o Click", true);

	}

	public void cliar_escrever() throws ElementFindException, GenericException {
		waitUntilElementExists(btnJsPromt);
		btnJsPromt.click();
		getDriver_().switchTo().alert().sendKeys("vitor");
		getDriver_().switchTo().alert().accept();
		Assert.assertTrue(elementExists(resultResporta));

	}

}
