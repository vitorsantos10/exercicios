package org.br.curso.pages;

import br.com.santander.frm.base.PageBase;
import br.com.santander.frm.base.VirtualElement;
import br.com.santander.frm.exceptions.ElementFindException;
import br.com.santander.frm.helpers.LoggerHelper;

import static br.com.santander.frm.helpers.QueryHelper.getElementByXPath;

import org.testng.Assert;

public class CriarUsuarioPage extends PageBase {

	VirtualElement homeUsuario = getElementByXPath("//h5[@class = \"center\"]"),
			nome = getElementByXPath("//input[@id = \"user_name\"]"),
			ultimoNome = getElementByXPath("//input[@id = \"user_lastname\"]"),
			Email = getElementByXPath("//input[@id = \"user_email\"]"),
			address = getElementByXPath("//input[@id = \"user_address\"]"),
			Universidade = getElementByXPath("//input[@id = \"user_university\"]"),
			profiss�o = getElementByXPath("//input[@id = \"user_profile\"]"),
			gen = getElementByXPath("//input[@id = \"user_gender\"]"),
			age = getElementByXPath("//input[@id = \"user_age\"]"),
			btnCriar = getElementByXPath("//input[@type = \"submit\"]");

	LoggerHelper logger = new LoggerHelper(BatistaHomePage.class);

	public void validar_homeUsuario() {
		Assert.assertTrue(elementExists(homeUsuario));
		logger.info("P�gina de login foi encontrada.", true);
	}

	public void preencher_formulario(String name, String ultimonome, String email, String endereco, String universidade,
			String profissao, String genero,String idade) throws ElementFindException {
		if (elementExists(nome) && elementExists(ultimoNome) && elementExists(Email) && elementExists(address)
				&& elementExists(Universidade) && elementExists(profiss�o) && elementExists(gen) && elementExists(age)) {

			nome.sendKeys(name);
			ultimoNome.sendKeys(ultimonome);
			Email.sendKeys(email);
			address.sendKeys(endereco);
			Universidade.sendKeys(universidade);
			profiss�o.sendKeys(profissao);
			gen.sendKeys(genero);
			age.sendKeys(idade);

			btnCriar.click();

			logger.info("Campos preenchidos", true);

		} else {
			logger.error("Os campos n�o foram preenchidos corretamente", true);
		}

	}

}
