package org.br.curso.features;

import static br.com.santander.frm.bdd.Gherkin.given_;

import java.util.concurrent.ExecutionException;

import org.apache.commons.exec.ExecuteException;

import br.com.santander.frm.bdd.Feature;
import br.com.santander.frm.bdd.Scenario;

@Feature("Drag")
public class DragAndDropFeature {

	@SuppressWarnings("static-access")
	@Scenario("CT004-Drag")
	public void CriarUsuarioFeature() throws ExecutionException {

		given_("Dado que acessei a home").when_("Quando clicar em interacoes").then_("Ent�o fazer DragAndDrop")
				.execute_();

	}

}