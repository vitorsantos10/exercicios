package org.br.curso.features;

import static br.com.santander.frm.bdd.Gherkin.given_;

import java.util.concurrent.ExecutionException;

import org.apache.commons.exec.ExecuteException;

import br.com.santander.frm.bdd.Feature;
import br.com.santander.frm.bdd.Scenario;

@Feature("Basic")
public class BasicAuthFeature {

	@SuppressWarnings("static-access")
	@Scenario("CT005-Basic")
	public void CriarUsuarioFeature() throws ExecutionException {

		given_("Dado que estou na HOME batista ").when_("Quando que os campos foram preenchidos")
				.then_("Ent�o o usuario sera autenticado").execute_();

	}

}