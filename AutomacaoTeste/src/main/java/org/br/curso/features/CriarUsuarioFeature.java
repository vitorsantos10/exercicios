package org.br.curso.features;

import static br.com.santander.frm.bdd.Gherkin.given_;

import java.util.concurrent.ExecutionException;

import org.apache.commons.exec.ExecuteException;

import br.com.santander.frm.bdd.Feature;
import br.com.santander.frm.bdd.Scenario;

@Feature("Usuario")
public class CriarUsuarioFeature {

	@SuppressWarnings("static-access")
	@Scenario("CT001-Usuario")
	public void CriarUsuarioFeature() throws ExecutionException {

		given_("Dado que estou na p�gina Automacao Bastica").when_("Quando clicar em usuario e preencher os campos")
				.then_("Ent�o o usuario sera criado").execute_();

	}

}